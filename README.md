## PRTG Device Template for Cisco Switches stack status

This project is a custom device template that can be used to monitor the stack unit status in PRTG using the auto-discovery for simplified sensor creation.

## Download
A zip file containing the template and necessary additional files can be downloaded from the repository using the link below:
- [Latest Version of the Template](https://gitlab.com/PRTG/Device-Templates/cisco-stack/-/jobs/artifacts/master/download?job=PRTGDistZip)

## Installation Instructions
Please refer to INSTALL.md or refer to the following KB-Post:
- [How can I get PRTG to recognize Cisco switches in stack that utilize only 1 IP address?](https://kb.paessler.com/en/topic/41133)